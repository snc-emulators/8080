#include <cstdio>
#include "Cpu.h"

Word Cpu::GetRegisterPair(Byte lsb, Byte msb) {
  Word address = lsb;
  address |= (msb << 8);

  return address;
}

Word Cpu::GetRegisterPairBC() {
  return GetRegisterPair(C, B);
}

Word Cpu::GetRegisterPairDE() {
  return GetRegisterPair(E, D);
}

Word Cpu::GetRegisterPairHL() {
  return GetRegisterPair(L, H);
}

Cpu::Cpu() {
  Reset();
}

s32 Cpu::Execute(s32 cycles) {
  auto WriteRegisterToMemory = [&cycles, this](Byte value) {
    Word address = GetRegisterPairHL();

    WriteByte(cycles, address, value);
  };

  s32 initialCycles = cycles;

  while (cycles > 0) {
    Byte instruction = FetchByte(cycles);

    switch (instruction) {
      case MOV_A_B:
      {
        A = B;
      } break;
      case MOV_A_C:
      {
        A = C;
      }break;
      case MOV_A_D:
      {
        A = D;
      } break;
      case MOV_A_E:
      {
        A = E;
      } break;
      case MOV_A_H:
      {
        A = H;
      } break;
      case MOV_A_L:
      {
        A = L;
      } break;
      case MOV_A_M:
      {
        Word address = GetRegisterPairHL();

        A = ReadByte(cycles, address);
      } break;
      case MOV_B_A:
      {
        B = A;
      } break;
      case MOV_B_C:
      {
        B = C;
      } break;
      case MOV_B_D:
      {
        B = D;
      } break;
      case MOV_B_E:
      {
        B = E;
      } break;
      case MOV_B_H:
      {
        B = H;
      } break;
      case MOV_B_L:
      {
        B = L;
      } break;
      case MOV_B_M:
      {
        Word address = GetRegisterPairHL();

        B = ReadByte(cycles, address);
      } break;
      case MOV_C_A:
      {
        C = A;
      } break;
      case MOV_C_B:
      {
        C = B;
      } break;
      case MOV_C_D:
      {
        C = D;
      } break;
      case MOV_C_E:
      {
        C = E;
      } break;
      case MOV_C_H:
      {
        C = H;
      } break;
      case MOV_C_L:
      {
        C = L;
      } break;
      case MOV_C_M:
      {
        Word address = GetRegisterPairHL();

        C = ReadByte(cycles, address);
      } break;
      case MOV_D_A:
      {
        D = A;
      } break;
      case MOV_D_B:
      {
        D = B;
      } break;
      case MOV_D_C:
      {
        D = C;
      } break;
      case MOV_D_E:
      {
        D = E;
      } break;
      case MOV_D_H:
      {
        D = H;
      } break;
      case MOV_D_L:
      {
        D = L;
      } break;
      case MOV_D_M:
      {
        Word address = GetRegisterPairHL();

        D = ReadByte(cycles, address);
      } break;
      case MOV_E_A:
      {
        E = A;
      } break;
      case MOV_E_B:
      {
        E = B;
      } break;
      case MOV_E_C:
      {
        E = C;
      } break;
      case MOV_E_D:
      {
        E = D;
      } break;
      case MOV_E_H:
      {
        E = H;
      } break;
      case MOV_E_L:
      {
        E = L;
      } break;
      case MOV_E_M:
      {
        Word address = GetRegisterPairHL();

        E = ReadByte(cycles, address);
      } break;
      case MOV_H_A:
      {
        H = A;
      } break;
      case MOV_H_B:
      {
        H = B;
      } break;
      case MOV_H_C:
      {
        H = C;
      } break;
      case MOV_H_D:
      {
        H = D;
      } break;
      case MOV_H_E:
      {
        H = E;
      } break;
      case MOV_H_L:
      {
        H = L;
      } break;
      case MOV_H_M:
      {
        Word address = GetRegisterPairHL();

        H = ReadByte(cycles, address);
      } break;
      case MOV_L_A:
      {
        L = A;
      } break;
      case MOV_L_B:
      {
        L = B;
      } break;
      case MOV_L_C:
      {
        L = C;
      } break;
      case MOV_L_D:
      {
        L = D;
      } break;
      case MOV_L_E:
      {
        L = E;
      } break;
      case MOV_L_H:
      {
        L = H;
      } break;
      case MOV_L_M:
      {
        Word address = GetRegisterPairHL();

        L = ReadByte(cycles, address);
      } break;
      case MOV_M_A:
      {
        WriteRegisterToMemory(A);
      } break;
      case MOV_M_B:
      {
        WriteRegisterToMemory(B);
      } break;
      case MOV_M_C:
      {
        WriteRegisterToMemory(C);
      } break;
      case MOV_M_D:
      {
        WriteRegisterToMemory(D);
      } break;
      case MOV_M_E:
      {
        WriteRegisterToMemory(E);
      } break;
      case MOV_M_H:
      {
        WriteRegisterToMemory(H);
      } break;
      case MOV_M_L:
      {
        WriteRegisterToMemory(L);
      } break;
      case MVI_A:
      {
        Byte value = AddrImmediate(cycles);

        A = value;
      } break;
      case MVI_B:
      {
        Byte value = AddrImmediate(cycles);

        B = value;
      } break;
      case MVI_C:
      {
        Byte value = AddrImmediate(cycles);

        C = value;
      } break;
      case MVI_D:
      {
        Byte value = AddrImmediate(cycles);

        D = value;
      } break;
      case MVI_E: {
        Byte value = AddrImmediate(cycles);

        E = value;
      } break;
      case MVI_H:
      {
        Byte value = AddrImmediate(cycles);

        H = value;
      } break;
      case MVI_L:
      {
        Byte value = AddrImmediate(cycles);

        L = value;
      } break;
      case MVI_M:
      {
        Byte value = AddrImmediate(cycles);

        WriteRegisterToMemory(value);
      } break;
      case LDA:
      {
        Word addr = FetchWord(cycles);
        Byte value = ReadByte(cycles, addr);

        A = value;
      } break;
      case LDAX_BC:
      {
        Word addr = GetRegisterPairBC();
        Byte value = ReadByte(cycles, addr);

        A = value;
      } break;
      case LDAX_DE:
      {
        Word addr = GetRegisterPairDE();
        Byte value = ReadByte(cycles, addr);

        A = value;
      } break;
      case LHLD:
      {
        Word addr = FetchWord(cycles);
        L = ReadByte(cycles, addr);
        H = ReadByte(cycles, addr + 1);
      } break;
      case STA:
      {
        Word addr = FetchWord(cycles);

        WriteByte(cycles, addr, A);
      } break;
      case STAX_BC:
      {
        Word addr = GetRegisterPairBC();

        WriteByte(cycles, addr, A);
      } break;
      case STAX_DE:
      {
        Word addr = GetRegisterPairDE();

        WriteByte(cycles, addr, A);
      } break;
      case SHLD:
      {
        Word addr = FetchWord(cycles);

        WriteByte(cycles, addr, L);
        WriteByte(cycles, addr + 1, H);
      } break;
      case XCHG:
      {
        // Swaps values in Registers L and E
        L = L ^ E;
        E = L ^ E;
        L = L ^ E;

        // Swaps values in Registers H and D
        H = H ^ D;
        D = H ^ D;
        H = H ^ D;
      } break;
      default:
        cycles--;
        std::printf("Invalid instruction %d", instruction);
        break;
    }
  }

  return initialCycles - cycles;
}

Byte Cpu::FetchByte(s32 &cycles) {
  Byte value = ReadByte(cycles, PC);
  PC++;

  return value;
}

Word Cpu::FetchWord(s32 &cycles) {
  Byte lsb = FetchByte(cycles);
  Byte msb = FetchByte(cycles);

  Word value = lsb;
  value |= (msb << 8);

  return value;
}

Byte Cpu::ReadByte(s32 &cycles, Word address) {
  Byte value = PROM[address];
  cycles--;

  return value;
}

void Cpu::Reset() {
  Reset(0);
}

void Cpu::Reset(Word resetVector) {
  // Processor State
  PC = resetVector;
  PS = 0;
  SP = 0;

  // Registers
  A = 0;
  B = 0;
  C = 0;
  D = 0;
  E = 0;
  H = 0;
  L = 0;
}

void Cpu::WriteByte(s32 &cycles, Word address, Byte value) {
  PROM[address] = value;
  cycles--;
}

Byte Cpu::AddrImmediate(s32 &cycles) {
  Byte value = ReadByte(cycles, PC);
  PC++;

  return value;
}