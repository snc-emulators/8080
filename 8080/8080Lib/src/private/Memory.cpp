#include "Memory.h"

Memory::Memory() {
  Reset();
}

void Memory::Reset() {
  for (unsigned char & i : Data) {
    i = 0;
  }
}