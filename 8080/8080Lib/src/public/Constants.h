#include "Types.h"

#ifndef INC_8080_CONSTANTS_H
#define INC_8080_CONSTANTS_H

// Device Configuration
static const u32 MAX_MEM = 1024 * 64;

  //  Instruction Opcodes
  //  Comment key:
  //  INSTRUCTION:
  //  VAR = Pattern
  //  MOV:
  //  MOV_D_S = 0b01DDDSSS
  //  MOV_A_S
const Byte
      MOV_A_B = 0b01111000,
      MOV_A_C = 0b01111001,
      MOV_A_D = 0b01111010,
      MOV_A_E = 0b01111011,
      MOV_A_H = 0b01111100,
      MOV_A_L = 0b01111101,
      MOV_A_M = 0b01111110,
  //  MOV_B_S
      MOV_B_A = 0b01000111,
      MOV_B_C = 0b01000001,
      MOV_B_D = 0b01000010,
      MOV_B_E = 0b01000011,
      MOV_B_H = 0b01000100,
      MOV_B_L = 0b01000101,
      MOV_B_M = 0b01000110,
  //  MOV_C_S
      MOV_C_A = 0b01001111,
      MOV_C_B = 0b01001000,
      MOV_C_D = 0b01001010,
      MOV_C_E = 0b01001011,
      MOV_C_H = 0b01001100,
      MOV_C_L = 0b01001101,
      MOV_C_M = 0b01001110,
  //  MOV_D_S
      MOV_D_A = 0b01010111,
      MOV_D_B = 0b01010000,
      MOV_D_C = 0b01010001,
      MOV_D_E = 0b01010011,
      MOV_D_H = 0b01010100,
      MOV_D_L = 0b01010101,
      MOV_D_M = 0b01010110,
  //  MOV_E_S
      MOV_E_A = 0b01011111,
      MOV_E_B = 0b01011000,
      MOV_E_C = 0b01011001,
      MOV_E_D = 0b01011010,
      MOV_E_H = 0b01011100,
      MOV_E_L = 0b01011101,
      MOV_E_M = 0b01011110,
  //  MOV_H_S
      MOV_H_A = 0b01100111,
      MOV_H_B = 0b01100000,
      MOV_H_C = 0b01100001,
      MOV_H_D = 0b01100010,
      MOV_H_E = 0b01100011,
      MOV_H_L = 0b01100101,
      MOV_H_M = 0b01100110,
  //  MOV_L_S
      MOV_L_A = 0b01101111,
      MOV_L_B = 0b01101000,
      MOV_L_C = 0b01101001,
      MOV_L_E = 0b01101011,
      MOV_L_D = 0b01101010,
      MOV_L_H = 0b01101100,
      MOV_L_M = 0b01101110,
  //  MOV_M_S
      MOV_M_A = 0b01110111,
      MOV_M_B = 0b01110000,
      MOV_M_C = 0b01110001,
      MOV_M_D = 0b01110010,
      MOV_M_E = 0b01110011,
      MOV_M_H = 0b01110100,
      MOV_M_L = 0b01110101,
  //  MVI_R
      MVI_A = 0b00111110,
      MVI_B = 0b00000110,
      MVI_C = 0b00001110,
      MVI_D = 0b00010110,
      MVI_E = 0b00011110,
      MVI_H = 0b00100110,
      MVI_L = 0b00101110,
      MVI_M = 0b00110110,
  //  Load Operations
  //  Direct addressing
      LDA = 0b00111010,
      STA = 0b00110010,
      LHLD = 0b00101010,
      SHLD = 0b00100010,
  //  Register Pair addressing
      LDAX_BC = 0b00001010,
      STAX_BC = 0b00000010,
      LDAX_DE = 0b00011010,
      STAX_DE = 0b00010010,
  //  XCHG
      XCHG = 0b11101011;

#endif //INC_8080_CONSTANTS_H
