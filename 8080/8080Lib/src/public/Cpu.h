#include "Types.h"
#include "Memory.h"

#ifndef M8080EMULATOR_M8080_H
#define M8080EMULATOR_M8080_H
class Cpu {
private:
  Word GetRegisterPair(Byte lsb, Byte msb);
public:
  /* Constructors */
  Cpu();

  /* Component properties */
  Memory PROM;

  /* Register properties */
  Byte A;
  Byte B;
  Byte C;
  Byte D;
  Byte E;
  Byte H;
  Byte L;

  Word PC;
  Word SP;

  union {
    Byte PS;
    ProcessorState Flags;
  };

  /* Methods */
  /* Addressing Modes */
  Byte AddrDirect(s32 &cycles);
  Byte AddrImmediate(s32 &cycles);
  Byte AddrIndirect(s32 &cycles);
  Byte AddrRegister(s32 &cycles);
  Byte AddrRegisterPair(s32 &cycles);

  /* Internal operations */
  s32 Execute(s32 cycles);
  Byte FetchByte(s32 &cycles);
  Word FetchWord (s32 &cycles);
  Word GetRegisterPairBC();
  Word GetRegisterPairDE();
  Word GetRegisterPairHL();
  Byte ReadByte(s32 &cycles, Word address);
  void Reset();
  void Reset(Word resetVector);
  void WriteByte(s32 &cycles, Word address, Byte value);
};

#endif //M8080EMULATOR_M8080_H