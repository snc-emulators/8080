#include "Types.h"
#include "Constants.h"

#ifndef INC_8080_MEMORY_H
#define INC_8080_MEMORY_H

class Memory {
protected:
  Byte Data[MAX_MEM];
public:
  Memory();
  void Reset();

  Byte& operator[](u32 address) {
    return Data[address];
  }
};

#endif //INC_8080_MEMORY_H
