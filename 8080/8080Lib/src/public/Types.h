#ifndef INC_8080_TYPES_H
#define INC_8080_TYPES_H

typedef int s32;
typedef unsigned int u32;
typedef unsigned char Byte;
typedef unsigned short int Word;
struct ProcessorState {
  Byte C : 1;         // BIT 0: Carry
  Byte AlwaysOn: 1;   // BIT 1: Always on
  Byte P : 1;         // BIT 2: Parity bit
  Byte AlwaysOff: 1;  // BIT 3: Always off
  Byte AC : 1;        // BIT 4: Auxiliary Carry
  Byte Unused: 1;     // BIT 5: Always off
  Byte Z : 1;         // BIT 6: Zero bit
  Byte S : 1;         // BIT 7: Sign bit
};

#endif //INC_8080_TYPES_H
