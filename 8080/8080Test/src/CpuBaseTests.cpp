#include "catch2/catch_test_macros.hpp"
#include "Cpu.h"
#include "Constants.h"

void ResetSuccessTest(Cpu cpu, Word expectedResetVector) {
  REQUIRE(cpu.PC == expectedResetVector);
  REQUIRE(cpu.PS == 0);
  REQUIRE(cpu.SP == 0);
  REQUIRE(cpu.A == 0);
  REQUIRE(cpu.B == 0);
  REQUIRE(cpu.C == 0);
  REQUIRE(cpu.D == 0);
  REQUIRE(cpu.E == 0);
  REQUIRE(cpu.H == 0);
  REQUIRE(cpu.L == 0);

  Byte hasNonZeroValues = 0x00;
  for (u32 i = 0; i < MAX_MEM; i++) {
    hasNonZeroValues |= cpu.PROM[i];
  }

  REQUIRE_FALSE(hasNonZeroValues);
}

void ResetSuccessTest(Cpu cpu) {
  ResetSuccessTest(cpu, 0);
}

TEST_CASE("CPU Tests") {
  Cpu cpu;
  SECTION("Constructor") {
    GIVEN("An instance is created") {
      THEN("Reset should have been called with default values") {
        ResetSuccessTest(cpu);
      }
    }
  }

  SECTION("Execute") {
    GIVEN("0 cycles passed") {
      s32 expectedExecutionCycles = 0;

      WHEN("Execute is called") {
        s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);

        THEN("No execution should take place") {
          REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        }
      }
    }

    GIVEN("An invalid instruction is passed") {
      s32 expectedExecutionCycles = 2;

      WHEN("Execute is called") {
        s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);

        THEN("A cycle is consumed for fetching of instruction and execution of instruction") {
          REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        }
      }
    }

    GIVEN("A valid instruction is passed") {
      s32 expectedExecutionCycles = 1;
      cpu.PROM[0] = MOV_A_B;

      WHEN("Execute is called") {
        s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);

        THEN("The correct number of cycles are consumed") {
          REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        }
      }
    }

    GIVEN("Less cycles than required are passed for an instruction") {
      s32 expectedExecutionCycles = 2;
      s32 passedExecutionCycles = 1;
      cpu.PROM[0] = MVI_A;
      cpu.PROM[1] = 0xFA;

      WHEN("Execute is called") {
        s32 actualExecutionCycles = cpu.Execute(passedExecutionCycles);
        THEN("The number of cycles consumed is returned") {
          REQUIRE(passedExecutionCycles != actualExecutionCycles);
          REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        }
      }

    }
  }

  SECTION("Reset method") {
    GIVEN("Reset called") {
      WHEN("No reset vector passed") {
        cpu.Reset();
        THEN("The Program Counter should be set to 0 and Memory should be initialised") {
          ResetSuccessTest(cpu);
        }
      }

      WHEN("Reset vector passed") {
        Word expectedResetVector = 0x4AFD;
        cpu.Reset(expectedResetVector);
        THEN("The Program Counter should be set to the expected value and Memory should be initialised") {
          ResetSuccessTest(cpu, expectedResetVector);
        }
      }
    }
  }
}