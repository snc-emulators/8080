#include "catch2/catch_test_macros.hpp"
#include "Memory.h"
#include "Constants.h"
#include <cstdio>

TEST_CASE("Memory Tests") {
  Memory mem;

  mem.Reset();

  SECTION("Operator [] tests") {
    GIVEN("An address is provided") {
      u32 address = 0xFFFC;
      u32 expectedValue = 0xAF;
      WHEN("A value is passed") {
        mem[address] = expectedValue;

        THEN("The address should contain the passed value") {
          REQUIRE(expectedValue == mem[address]);
        }
      }
    }
  }
}