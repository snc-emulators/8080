#include "catch2/catch_test_macros.hpp"
#include "Cpu.h"

class RegisterTest {
protected:
  Cpu cpu;
public:
  void DirectAddressingTest(Byte opcode, Byte Cpu::*targetRegister);
  void ImmediateAddressingTest(Byte opcode, Byte Cpu::*targetRegister);
  void TransferAddressValueTest(Byte opcode, Byte Cpu::*targetRegister);
  void TransferRegisterATest(Byte opcode, Byte Cpu::*targetRegister);
  void TransferRegisterBTest(Byte opcode, Byte Cpu::*targetRegister);
  void TransferRegisterCTest(Byte opcode, Byte Cpu::*targetRegister);
  void TransferRegisterDTest(Byte opcode, Byte Cpu::*targetRegister);
  void TransferRegisterETest(Byte opcode, Byte Cpu::*targetRegister);
  void TransferRegisterHTest(Byte opcode, Byte Cpu::*targetRegister);
  void TransferRegisterLTest(Byte opcode, Byte Cpu::*targetRegister);
  void TransferRegisterToMemory(Byte opcode, Byte Cpu::*sourceRegister);
};

void RegisterTest::ImmediateAddressingTest(Byte opcode, Byte Cpu::*targetRegister) {
  // Setup:
  s32 expectedExecutionCycles = 2;
  Byte expectedValue = 0xEF;

  cpu.PROM[0] = opcode;
  cpu.PROM[1] = expectedValue;

  // Execute:
  s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);
  Byte actualValue = cpu.*targetRegister;

  // Assert
  REQUIRE(expectedExecutionCycles == actualExecutionCycles);
  REQUIRE(expectedValue == actualValue);
}

void RegisterTest::TransferAddressValueTest(Byte opcode, Byte Cpu::*targetRegister) {
  // Setup:
  s32 expectedExecutionCycles = 2;
  Byte expectedValue = 0xEF;
  cpu.H = 0x30;
  cpu.L = 0xA3;
  cpu.*targetRegister = 0;
  cpu.PROM[0] = opcode;
  cpu.PROM[0x30A3] = expectedValue;

  s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);

  REQUIRE(expectedExecutionCycles == actualExecutionCycles);
  REQUIRE(expectedValue == cpu.*targetRegister);
}

void RegisterTest::TransferRegisterATest(Byte opcode, Byte Cpu::*targetRegister) {
  // Setup:
  s32 expectedExecutionCycles = 1;
  cpu.A = 0xAE;
  cpu.*targetRegister = 0;
  cpu.PROM[0] = opcode;

  // Execution:
  s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);

  // Assertion:
  REQUIRE(expectedExecutionCycles == actualExecutionCycles);
  REQUIRE(cpu.A == cpu.*targetRegister);
}


void RegisterTest::TransferRegisterBTest(Byte opcode, Byte Cpu::*targetRegister) {
  // Setup:
  s32 expectedExecutionCycles = 1;
  cpu.B = 0x0D;
  cpu.*targetRegister = 0;
  cpu.PROM[0] = opcode;

  // Execution:
  s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);

  // Assertion:
  REQUIRE(expectedExecutionCycles == actualExecutionCycles);
  REQUIRE(cpu.B == cpu.*targetRegister);
}

void RegisterTest::TransferRegisterCTest(Byte opcode, Byte Cpu::*targetRegister) {
  // Setup:
  s32 expectedExecutionCycles = 1;
  cpu.C = 0x92;
  cpu.*targetRegister = 0;
  cpu.PROM[0] = opcode;

  // Execution:
  s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);

  // Assertion:
  REQUIRE(expectedExecutionCycles == actualExecutionCycles);
  REQUIRE(cpu.C == cpu.*targetRegister);
}

void RegisterTest::TransferRegisterDTest(Byte opcode, Byte Cpu::*targetRegister) {
  // Setup:
  s32 expectedExecutionCycles = 1;
  cpu.D = 0xFA;
  cpu.*targetRegister = 0;
  cpu.PROM[0] = opcode;

  // Execution:
  s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);

  // Assertion:
  REQUIRE(expectedExecutionCycles == actualExecutionCycles);
  REQUIRE(cpu.D == cpu.*targetRegister);
}

void RegisterTest::TransferRegisterETest(Byte opcode, Byte Cpu::*targetRegister) {
  // Setup:
  s32 expectedExecutionCycles = 1;
  cpu.D = 0x4F;
  cpu.*targetRegister = 0;
  cpu.PROM[0] = opcode;

  // Execution:
  s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);

  // Assertion:
  REQUIRE(expectedExecutionCycles == actualExecutionCycles);
  REQUIRE(cpu.E == cpu.*targetRegister);
}

void RegisterTest::TransferRegisterHTest(Byte opcode, Byte Cpu::*targetRegister) {
  // Setup:
  s32 expectedExecutionCycles = 1;
  cpu.H = 0x5C;
  cpu.*targetRegister = 0;
  cpu.PROM[0] = opcode;

  // Execution:
  s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);

  // Assertion:
  REQUIRE(expectedExecutionCycles == actualExecutionCycles);
  REQUIRE(cpu.H == cpu.*targetRegister);
}

void RegisterTest::TransferRegisterLTest(Byte opcode, Byte Cpu::*targetRegister) {
  // Setup:
  s32 expectedExecutionCycles = 1;
  cpu.L = 0xAA;
  cpu.*targetRegister = 0;
  cpu.PROM[0] = opcode;

  // Execution:
  s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);

  // Assertion:
  REQUIRE(expectedExecutionCycles == actualExecutionCycles);
  REQUIRE(cpu.L == cpu.*targetRegister);
}

void RegisterTest::TransferRegisterToMemory(Byte opcode, Byte Cpu::*sourceRegister) {
  // Setup:
  s32 expectedExecutionCycles = 2;
  cpu.L = 0xBC;
  cpu.H = 0xFA;
  cpu.*sourceRegister = 0xA1;
  cpu.PROM[0] = opcode;
  cpu.PROM[0xFABC] = 0;

  // Execution:
  s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);
  Byte actualValue = cpu.PROM[0xFABC];

  // Assertion
  REQUIRE(expectedExecutionCycles == actualExecutionCycles);
  REQUIRE(cpu.*sourceRegister == actualValue);
}

TEST_CASE("Register Pairs") {
  Cpu cpu;
  GIVEN("Register Pair BC") {
    WHEN("MSB and LSB contain 0") {
      cpu.B = 0;
      cpu.C = 0;

      THEN("Register Pair should be 0") {
        Word actual = cpu.GetRegisterPairBC();

        REQUIRE(0x0 == actual);
      }
    }

    WHEN("MSB contains 0xFF") {
      cpu.B = 0xFF;
      THEN("Register Pair should contain 0xFF00") {
        Word actual = cpu.GetRegisterPairBC();

        REQUIRE(0xFF00 == actual);
      }
    }

    WHEN("LSB contains 0xFF") {
      cpu.C = 0xFF;
      THEN("Register Pair should contain 0x00FF") {
        Word actual = cpu.GetRegisterPairBC();

        REQUIRE(0x00FF == actual);
      }
    }

    WHEN("MSB and LSB contain 0xFF") {
      cpu.B = 0xFF;
      cpu.C = 0xFF;
      THEN("Register Pair should contain 0xFFFF") {
        Word actual = cpu.GetRegisterPairBC();

        REQUIRE(0xFFFF == actual);
      }
    }
  }

  GIVEN("Register Pair DE") {
    WHEN("MSB and LSB contain 0") {
      cpu.D = 0;
      cpu.E = 0;

      THEN("Register Pair should be 0") {
        Word actual = cpu.GetRegisterPairDE();

        REQUIRE(0x0 == actual);
      }
    }

    WHEN("MSB contains 0xFF") {
      cpu.D = 0xFF;
      THEN("Register Pair should contain 0xFF00") {
        Word actual = cpu.GetRegisterPairDE();

        REQUIRE(0xFF00 == actual);
      }
    }

    WHEN("LSB contains 0xFF") {
      cpu.E = 0xFF;
      THEN("Register Pair should contain 0x00FF") {
        Word actual = cpu.GetRegisterPairDE();

        REQUIRE(0x00FF == actual);
      }
    }

    WHEN("MSB and LSB contain 0xFF") {
      cpu.D = 0xFF;
      cpu.E = 0xFF;
      THEN("Register Pair should contain 0xFFFF") {
        Word actual = cpu.GetRegisterPairDE();

        REQUIRE(0xFFFF == actual);
      }
    }
  }

  GIVEN("Register Pair HL") {
    WHEN("MSB and LSB contain 0") {
      cpu.H = 0;
      cpu.L = 0;

      THEN("Register Pair should be 0") {
        Word actual = cpu.GetRegisterPairHL();

        REQUIRE(0x0 == actual);
      }
    }

    WHEN("MSB contains 0xFF") {
      cpu.H = 0xFF;
      THEN("Register Pair should contain 0xFF00") {
        Word actual = cpu.GetRegisterPairHL();

        REQUIRE(0xFF00 == actual);
      }
    }

    WHEN("LSB contains 0xFF") {
      cpu.L = 0xFF;
      THEN("Register Pair should contain 0x00FF") {
        Word actual = cpu.GetRegisterPairHL();

        REQUIRE(0x00FF == actual);
      }
    }

    WHEN("MSB and LSB contain 0xFF") {
      cpu.H = 0xFF;
      cpu.L = 0xFF;
      THEN("Register Pair should contain 0xFFFF") {
        Word actual = cpu.GetRegisterPairHL();

        REQUIRE(0xFFFF == actual);
      }
    }
  }
}

TEST_CASE_METHOD(RegisterTest, "Register Operations") {
  GIVEN("Transfer to Accumulator") {
    Byte Cpu::*targetRegister = &Cpu::A;

    WHEN("Source is Register B") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register B") {
        TransferRegisterBTest(MOV_A_B, targetRegister);
      }
    }

    WHEN("Source is Register C") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register C") {
        TransferRegisterCTest(MOV_A_C, targetRegister);
      }
    }

    WHEN("Source is Register D") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register D") {
        TransferRegisterDTest(MOV_A_D, targetRegister);
      }
    }

    WHEN("Source is Register E") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register E") {
        TransferRegisterETest(MOV_A_E, targetRegister);
      }
    }

    WHEN("Source is Register H") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register H") {
        TransferRegisterHTest(MOV_A_H, targetRegister);
      }
    }

    WHEN("Source is Register L") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register L") {
        TransferRegisterLTest(MOV_A_L, targetRegister);
      }
    }

    WHEN("Source is Memory") {
      THEN("Instruction consumes 2 cycles and sets target register to value of Address 0xHHHHLLLL") {
        TransferAddressValueTest(MOV_A_M, targetRegister);
      }
    }

    WHEN("Source is Instruction Byte 2") {
      THEN("Instruction consumes 3 cycles and sets target register to value of Byte 2 of Instruction") {
        ImmediateAddressingTest(MVI_A, targetRegister);
      }
    }
  }

  GIVEN("Transfer to Register B") {
    Byte Cpu::*targetRegister = &Cpu::B;

    WHEN("Source is Register A") {
      THEN("Instruction consumes 1 cycles and sets target register to value of Register A") {
        TransferRegisterATest(MOV_B_A, targetRegister);
      }
    }

    WHEN("Source is Register C") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register C") {
        TransferRegisterCTest(MOV_B_C, targetRegister);
      }
    }

    WHEN("Source is Register D") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register D") {
        TransferRegisterDTest(MOV_B_D, targetRegister);
      }
    }

    WHEN("Source is Register E") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register E") {
        TransferRegisterETest(MOV_B_E, targetRegister);
      }
    }

    WHEN("Source is Register H") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register H") {
        TransferRegisterHTest(MOV_B_H, targetRegister);
      }
    }

    WHEN("Source is Register L") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register L") {
        TransferRegisterLTest(MOV_B_L, targetRegister);
      }
    }

    WHEN("Source is Memory") {
      THEN("Instruction consumes 2 cycles and sets target register to value of Address 0xHHHHLLLL") {
        TransferAddressValueTest(MOV_B_M, targetRegister);
      }
    }

    WHEN("Source is Instruction Byte 2") {
      THEN("Instruction consumes 3 cycles and sets target register to value of Byte 2 of Instruction") {
        ImmediateAddressingTest(MVI_B, targetRegister);
      }
    }
  }

  GIVEN("Transfer to Register C") {
    Byte Cpu::*targetRegister = &Cpu::C;

    WHEN("Source is Register A") {
      THEN("Instruction consumes 1 cycles and sets target register to value of Register A") {
        TransferRegisterATest(MOV_C_A, targetRegister);
      }
    }

    WHEN("Source is Register B") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register B") {
        TransferRegisterBTest(MOV_C_B, targetRegister);
      }
    }

    WHEN("Source is Register D") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register D") {
        TransferRegisterDTest(MOV_C_D, targetRegister);
      }
    }

    WHEN("Source is Register E") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register E") {
        TransferRegisterETest(MOV_C_E, targetRegister);
      }
    }

    WHEN("Source is Register H") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register H") {
        TransferRegisterHTest(MOV_C_H, targetRegister);
      }
    }

    WHEN("Source is Register L") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register L") {
        TransferRegisterLTest(MOV_C_L, targetRegister);
      }
    }

    WHEN("Source is Memory") {
      THEN("Instruction consumes 2 cycles and sets target register to value of Address 0xHHHHLLLL") {
        TransferAddressValueTest(MOV_C_M, targetRegister);
      }
    }

    WHEN("Source is Instruction Byte 2") {
      THEN("Instruction consumes 3 cycles and sets target register to value of Byte 2 of Instruction") {
        ImmediateAddressingTest(MVI_C, targetRegister);
      }
    }
  }

  GIVEN("Transfer to Register D") {
    Byte Cpu::*targetRegister = &Cpu::D;

    WHEN("Source is Register A") {
      THEN("Instruction consumes 1 cycles and sets target register to value of Register A") {
        TransferRegisterATest(MOV_D_A, targetRegister);
      }
    }

    WHEN("Source is Register B") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register B") {
        TransferRegisterBTest(MOV_D_B, targetRegister);
      }
    }

    WHEN("Source is Register C") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register C") {
        TransferRegisterCTest(MOV_D_C, targetRegister);
      }
    }

    WHEN("Source is Register E") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register E") {
        TransferRegisterETest(MOV_D_E, targetRegister);
      }
    }

    WHEN("Source is Register H") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register H") {
        TransferRegisterHTest(MOV_D_H, targetRegister);
      }
    }

    WHEN("Source is Register L") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register L") {
        TransferRegisterLTest(MOV_D_L, targetRegister);
      }
    }

    WHEN("Source is Memory") {
      THEN("Instruction consumes 2 cycles and sets target register to value of Address 0xHHHHLLLL") {
        TransferAddressValueTest(MOV_D_M, targetRegister);
      }
    }

    WHEN("Source is Instruction Byte 2") {
      THEN("Instruction consumes 3 cycles and sets target register to value of Byte 2 of Instruction") {
        ImmediateAddressingTest(MVI_D, targetRegister);
      }
    }
  }

  GIVEN("Transfer to Register E") {
    Byte Cpu::*targetRegister = &Cpu::E;

    WHEN("Source is Register A") {
      THEN("Instruction consumes 1 cycles and sets target register to value of Register A") {
        TransferRegisterATest(MOV_E_A, targetRegister);
      }
    }

    WHEN("Source is Register B") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register B") {
        TransferRegisterBTest(MOV_E_B, targetRegister);
      }
    }

    WHEN("Source is Register C") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register C") {
        TransferRegisterCTest(MOV_E_C, targetRegister);
      }
    }

    WHEN("Source is Register D") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register D") {
        TransferRegisterDTest(MOV_E_D, targetRegister);
      }
    }

    WHEN("Source is Register H") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register H") {
        TransferRegisterHTest(MOV_E_H, targetRegister);
      }
    }

    WHEN("Source is Register L") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register L") {
        TransferRegisterLTest(MOV_E_L, targetRegister);
      }
    }

    WHEN("Source is Memory") {
      THEN("Instruction consumes 2 cycles and sets target register to value of Address 0xHHHHLLLL") {
        TransferAddressValueTest(MOV_E_M, targetRegister);
      }
    }

    WHEN("Source is Instruction Byte 2") {
      THEN("Instruction consumes 3 cycles and sets target register to value of Byte 2 of Instruction") {
        ImmediateAddressingTest(MVI_E, targetRegister);
      }
    }
  }

  GIVEN("Transfer to Register H") {
    Byte Cpu::*targetRegister = &Cpu::H;

    WHEN("Source is Register A") {
      THEN("Instruction consumes 1 cycles and sets target register to value of Register A") {
        TransferRegisterATest(MOV_H_A, targetRegister);
      }
    }

    WHEN("Source is Register B") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register B") {
        TransferRegisterBTest(MOV_H_B, targetRegister);
      }
    }

    WHEN("Source is Register C") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register C") {
        TransferRegisterCTest(MOV_H_C, targetRegister);
      }
    }

    WHEN("Source is Register D") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register D") {
        TransferRegisterDTest(MOV_H_D, targetRegister);
      }
    }

    WHEN("Source is Register E") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register E") {
        TransferRegisterETest(MOV_H_E, targetRegister);
      }
    }

    WHEN("Source is Register L") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register L") {
        TransferRegisterLTest(MOV_H_L, targetRegister);
      }
    }

    WHEN("Source is Memory") {
      THEN("Instruction consumes 2 cycles and sets target register to value of Address 0xHHHHLLLL") {
        // Setup:
        s32 expectedExecutionCycles = 2;
        Byte expectedValue = 0xEF;
        cpu.H = 0x30;
        cpu.L = 0xA3;
        cpu.PROM[0] = MOV_H_M;
        cpu.PROM[0x30A3] = expectedValue;

        s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);

        REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        REQUIRE(expectedValue == cpu.*targetRegister);
      }
    }

    WHEN("Source is Instruction Byte 2") {
      THEN("Instruction consumes 3 cycles and sets target register to value of Byte 2 of Instruction") {
        ImmediateAddressingTest(MVI_H, targetRegister);
      }
    }
  }

  GIVEN("Transfer to Register L") {
    Byte Cpu::*targetRegister = &Cpu::L;

    WHEN("Source is Register A") {
      THEN("Instruction consumes 1 cycles and sets target register to value of Register A") {
        TransferRegisterATest(MOV_L_A, targetRegister);
      }
    }

    WHEN("Source is Register B") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register B") {
        TransferRegisterBTest(MOV_L_B, targetRegister);
      }
    }

    WHEN("Source is Register C") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register C") {
        TransferRegisterCTest(MOV_L_C, targetRegister);
      }
    }

    WHEN("Source is Register D") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register C") {
        TransferRegisterDTest(MOV_L_D, targetRegister);
      }
    }

    WHEN("Source is Register E") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register C") {
        TransferRegisterETest(MOV_L_E, targetRegister);
      }
    }

    WHEN("Source is Register H") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register C") {
        TransferRegisterHTest(MOV_L_H, targetRegister);
      }
    }

    WHEN("Source is Memory") {
      THEN("Instruction consumes 2 cycles and sets target register to value of Address 0xHHHHLLLL") {
        // Setup:
        s32 expectedExecutionCycles = 2;
        Byte expectedValue = 0xEF;
        cpu.H = 0x30;
        cpu.L = 0xA3;
        cpu.PROM[0] = MOV_L_M;
        cpu.PROM[0x30A3] = expectedValue;

        s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);

        REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        REQUIRE(expectedValue == cpu.*targetRegister);
      }
    }

    WHEN("Source is Instruction Byte 2") {
      THEN("Instruction consumes 3 cycles and sets target register to value of Byte 2 of Instruction") {
        ImmediateAddressingTest(MVI_L, targetRegister);
      }
    }
  }
}

TEST_CASE_METHOD(RegisterTest, "Memory Operations") {
  GIVEN("Transfer to memory address") {
    WHEN("Source is Register A") {
      THEN("Instruction consumes 1 cycles and sets target register to value of Register A") {
        TransferRegisterToMemory(MOV_M_A, &Cpu::A);
      }
    }

    WHEN("Source is Register B") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register B") {
        TransferRegisterToMemory(MOV_M_B, &Cpu::B);
      }
    }

    WHEN("Source is Register C") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register C") {
        TransferRegisterToMemory(MOV_M_C, &Cpu::C);
      }
    }

    WHEN("Source is Register D") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register C") {
        TransferRegisterToMemory(MOV_M_D, &Cpu::D);
      }
    }

    WHEN("Source is Register E") {
      THEN("Instruction consumes 1 cycle and sets target register to value of Register C") {
        TransferRegisterToMemory(MOV_M_E, &Cpu::E);
      }
    }

    WHEN("Source is Register H") {
      // Setup:
      s32 expectedExecutionCycles = 2;
      cpu.L = 0xBC;
      cpu.H = 0xFA;
      cpu.PROM[0] = MOV_M_H;
      cpu.PROM[0xFABC] = 0;

      // Execution:
      s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);
      Byte actualValue = cpu.PROM[0xFABC];

      THEN("Instruction consumes 1 cycle and sets target register to value of Register C") {
        // Assertion
        REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        REQUIRE(cpu.H == actualValue);
      }
    }

    WHEN("Source is Register L") {
      // Setup:
      s32 expectedExecutionCycles = 2;
      cpu.L = 0xBC;
      cpu.H = 0xFA;
      cpu.PROM[0] = MOV_M_L;
      cpu.PROM[0xFABC] = 0;

      // Execution:
      s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);
      Byte actualValue = cpu.PROM[0xFABC];

      THEN("Instruction consumes 1 cycle and sets target register to value of Register C") {
        // Assertion
        REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        REQUIRE(cpu.L == actualValue);
      }
    }

    WHEN("Source is Instruction Byte 2") {
      // Setup:
      s32 expectedExecutionCycles = 3;
      Byte expectedValue = 0xCA;
      cpu.L = 0xAD;
      cpu.H = 0x10;
      cpu.PROM[0] = MVI_M;
      cpu.PROM[1] = expectedValue;

      s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);
      Byte actualValue = cpu.PROM[0x10AD];

      THEN("Instruction consumes 3 cycles and sets memory location 0xHL to value of Instruction Byte 2") {
        REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        REQUIRE(expectedValue == actualValue);
      }
    }
  }
}

TEST_CASE_METHOD(RegisterTest, "Load Operations") {
  GIVEN("Load Accumulator") {
    WHEN("Direct addressing mode") {
      s32 expectedExecutionCycles = 4;
      Byte expectedValue = 0xBF;
      cpu.PROM[0] = LDA;
      cpu.PROM[1] = 0x1F;
      cpu.PROM[2] = 0xDA;
      cpu.PROM[0xDA1F] = expectedValue;

      s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);
      Byte actualValue = cpu.A;

      THEN("Instruction consumes 4 cycles and sets value of Accumulator to value found in address ((IB3)(IB2))") {
        REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        REQUIRE(expectedValue == actualValue);
      }
    }

    WHEN("Register Pair addressing mode, BC") {
      s32 expectedExecutionCycles = 2;
      Byte expectedValue = 0xDB;
      cpu.B = 0x1A;
      cpu.C = 0x01;
      cpu.PROM[0] = LDAX_BC;
      cpu.PROM[0x1A01] = expectedValue;

      s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);
      Byte actualValue = cpu.A;

      THEN("Instruction consumes 2 cycles and sets value of accumulator to value found in address ((B)(C))") {
        REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        REQUIRE(expectedValue == actualValue);
      }
    }

    WHEN("Register Pair addressing mode, DE") {
      s32 expectedExecutionCycles = 2;
      Byte expectedValue = 0xDB;
      cpu.D = 0x1F;
      cpu.E = 0x2D;
      cpu.PROM[0] = LDAX_DE;
      cpu.PROM[0x1F2D] = expectedValue;

      s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);
      Byte actualValue = cpu.A;

      THEN("Instruction consumes 2 cycles and sets value of accumulator to value found in address ((D)(E))") {
        REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        REQUIRE(expectedValue == actualValue);
      }
    }
  }

  GIVEN("Load H and L") {
    WHEN("Direct addressing mode") {
      s32 expectedExecutionCycles = 5;
      Byte expectedValueL = 0xBF;
      Byte expectedValueH = 0xAD;
      cpu.PROM[0] = LHLD;
      cpu.PROM[1] = 0x1F;
      cpu.PROM[2] = 0xDA;
      cpu.PROM[0xDA1F] = expectedValueL;
      cpu.PROM[0xDA20] = expectedValueH;

      s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);
      Byte actualValueL = cpu.L;
      Byte actualValueH = cpu.H;

      THEN("Instruction consumes 4 cycles and sets value of LH to value found in address ((IB3)(IB2)) and ((IB3)(IB2)+1)") {
        REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        REQUIRE(expectedValueL == actualValueL);
        REQUIRE(expectedValueH == actualValueH);
      }
    }
  }
}

TEST_CASE_METHOD(RegisterTest, "Store Operations") {
  GIVEN("Store Accumulator") {
    WHEN("Direct addressing mode") {
      s32 expectedExecutionCycles = 4;
      cpu.A = 0x4A;
      cpu.PROM[0] = STA;
      cpu.PROM[1] = 0xA1;
      cpu.PROM[2] = 0xFA;
      cpu.PROM[0xFAA1] = 0;

      s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);
      Byte actualValue = cpu.PROM[0xFAA1];

      THEN("Instruction consumes 4 cycles and sets value found in address ((IB3)(IB2)) to value stored in Accumulator") {
        REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        REQUIRE(cpu.A == actualValue);
      }
    }

    GIVEN("Register Pair addressing mode") {
      WHEN("Register Pair BC") {
        s32 expectedExecutionCycles = 2;
        Byte expectedValue = 0xDB;
        cpu.A = expectedValue;
        cpu.B = 0x1A;
        cpu.C = 0x01;
        cpu.PROM[0] = STAX_BC;
        cpu.PROM[0x1A01] = 0;

        s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);
        Byte actualValue = cpu.PROM[0x1A01];

        THEN("Instruction consumes 2 cycles and sets value of accumulator to value found in address ((B)(C))") {
          REQUIRE(expectedExecutionCycles == actualExecutionCycles);
          REQUIRE(expectedValue == actualValue);
        }
      }

      WHEN("Register Pair DE") {
        s32 expectedExecutionCycles = 2;
        Byte expectedValue = 0xDB;
        cpu.A = expectedValue;
        cpu.D = 0x1F;
        cpu.E = 0x2D;
        cpu.PROM[0] = STAX_DE;
        cpu.PROM[0x1F2D] = 0;

        s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);
        Byte actualValue = cpu.PROM[0x1F2D];

        THEN("Instruction consumes 2 cycles and sets value of accumulator to value found in address ((D)(E))") {
          REQUIRE(expectedExecutionCycles == actualExecutionCycles);
          REQUIRE(expectedValue == actualValue);
        }
      }
    }
  }

  GIVEN("Store L & H") {
    WHEN("Direct addressing mode") {
      s32 expectedExecutionCycles = 5;
      cpu.L = 0xAF;
      cpu.H = 0xDA;
      cpu.PROM[0] = SHLD;
      cpu.PROM[1] = 0xFA;
      cpu.PROM[2] = 0xAD;
      cpu.PROM[0xADFA] = 0;

      s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);
      Byte actualValueLSB = cpu.PROM[0xADFA];
      Byte actualValueMSB = cpu.PROM[0xADFB];

      THEN("Instruction consumes 5 cycles and sets value of ((IB3)(IB2)) to value found in L and ((IB3)(IB2)+1) to value found in H") {
        REQUIRE(expectedExecutionCycles == actualExecutionCycles);
        REQUIRE(cpu.L == actualValueLSB);
        REQUIRE(cpu.H == actualValueMSB);
      }
    }
  }

  GIVEN("Exchange Register Pair") {
    WHEN("Executed") {
      s32 expectedExecutionCycles = 1;
      Byte originalLValue = 0xFA;
      Byte originalHValue = 0xAF;
      Byte originalDValue = 0xA1;
      Byte originalEValue = 0xBA;

      cpu.L = originalLValue;
      cpu.H = originalHValue;
      cpu.D = originalDValue;
      cpu.E = originalEValue;
      cpu.PROM[0] = XCHG;

      s32 actualExecutionCycles = cpu.Execute(expectedExecutionCycles);
      THEN("Instruction consumes 1 cycles and values found in RP DE and HL are exchanged") {
        REQUIRE(originalLValue == cpu.E);
        REQUIRE(originalEValue == cpu.L);
        REQUIRE(originalHValue == cpu.D);
        REQUIRE(originalDValue == cpu.H);
      }
    }
  }
}